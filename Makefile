start:
	export ENV="dev"; python src/app.py

lint:
	shopt -s globstar && black --check ./**/*.py

format:
	shopt -s globstar &&	black ./**/**/*.py

install:
	pip install -r ./requirements.txt
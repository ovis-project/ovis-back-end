# OVIS service

The back end service for the ovis project

![ovis logo](https://assets.gitlab-static.net/uploads/-/system/group/avatar/6901760/logo-full.png)


## Installation

- create a virtual env `virualenv ./.venv`
- Select the venv `source ./.venv/bin/activate`
- Install dependencies `make install`
- Run the service `make start`

## Features

`TODO: Add features`
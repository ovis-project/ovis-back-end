import os
from flask import Flask, request
from sqlalchemy import create_engine, event
from storage import SQLiteStrategy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import Engine
from flask_restful import Resource, Api
from flask_cors import CORS
import json
from flask_migrate import Migrate

from users import get_resource as get_user_resource
from trucks import get_trucks_resource
from people import get_partner_resource, get_driver_resource, get_truck_owner_resource
from licenses import (
    LicenseTypeController,
    get_license_type_resource,
    LicenseDocumentController,
    get_license_document_resource,
    # get_license_relation_resource
)


# TODO: import from configuration
dbfile = "/tmp/licenses-sqlite.db"
storage = SQLiteStrategy.get_instance()
SQLiteStrategy.initialize(dbfile)

app = Flask(__name__)
CORS(app)
api = Api(app)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///{}".format(dbfile)
migrate = Migrate(app, storage.get_instance().get_base())

"""
@api {get} /api Get application status
"""


@app.route("/api/")
def status():
    content = json.dumps({"status": "ok"})

    response = app.make_response((content, 200))
    response.headers["Content-Type"] = "application/json"
    return response


api.add_resource(
    get_user_resource(storage),
    "/api/users",
    "/api/users/<int:resource_id>",
    methods=["GET", "POST", "PUT"],
)

api.add_resource(
    get_license_type_resource(storage),
    "/api/license-types",
    "/api/license-types/<int:resource_id>",
    methods=["GET", "POST", "PUT", "DELETE"],
)

api.add_resource(
    get_license_document_resource(storage),
    "/api/licenses",
    "/api/licenses/<int:resource_id>",
    methods=["GET", "POST", "PUT", "DELETE"],
)

api.add_resource(
    get_partner_resource(storage),
    "/api/partners",
    "/api/partners/<int:resource_id>",
    methods=["GET", "POST", "PUT", "DELETE"],
)

api.add_resource(
    get_driver_resource(storage),
    "/api/drivers",
    "/api/drivers/<int:resource_id>/assign-license",
    "/api/drivers/<int:resource_id>",
    methods=["GET", "POST", "PUT", "DELETE"],
)

api.add_resource(
    get_truck_owner_resource(storage),
    "/api/truck-owners",
    "/api/truck-owners/<int:resource_id>/assign-license",
    "/api/truck-owners/<int:resource_id>",
    methods=["GET", "POST", "PUT", "DELETE"],
)

api.add_resource(
    get_trucks_resource(storage),
    "/api/trucks",
    "/api/trucks/<int:resource_id>/assign-license",
    "/api/trucks/<int:resource_id>",
    methods=["GET", "POST", "PUT", "DELETE"],
)

db_engine = storage.get_connection()
Base = storage.get_base()
Base.metadata.create_all(db_engine)


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


if __name__ == "__main__":
    app.debug = True

    if os.environ["ENV"] == "dev":
        app.run(host="0.0.0.0", port=5000)
    else:
        certificate = os.environ["TRUCKS_CERT"]
        key = os.environ["TRUCKS_KEY"]
        app.run(host="0.0.0.0", port=5000, ssl_context=(certificate, key))

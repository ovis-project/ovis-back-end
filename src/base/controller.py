"""
  Base.Controller has a common implementation for the controllers
"""

from sqlalchemy import MetaData
from datetime import datetime
from abc import ABC


class Controller(ABC):
    def __init__(self, resource, base, session, conn):

        self.resource = resource
        self.db_connection = conn

        if base is None:
            raise Exception("Base is None")

        if session is None:
            raise Exception("Session is None")

        self.session = session

    def create(self, **kwargs):
        now = datetime.now()
        instance = self.resource(**kwargs, updated_at=now, created_at=now)
        self.session.add(instance)
        self.session.commit()

        return instance

    def read_one(self, id):
        if id is None:
            raise ("id is none")

        if id < 0:
            raise ("id can not be null")

        return self.session.query(self.resource).filter(self.resource.id == id).one()

    def read_all(self, **kwargs):

        if kwargs["per_page"]:
            per_page = kwargs["per_page"]
        else:
            per_page = 10

        if kwargs["page"]:
            page = kwargs["page"]
            offset = (int(kwargs["page"]) - 1) * per_page
        else:
            offset = 0

        return self.session.query(self.resource).limit(per_page).offset(offset).all()

    def count_all(self):
        all_records = self.read_all(per_page=-1, page=1)
        return len(list(all_records))

    def update(self, id, **kwargs):
        if id is None:
            raise ("id is none")

        if id < 0:
            raise ("id can not be null")

        instance = self.session.query(self.resource).filter(self.resource.id == id)
        instance.one()

        kwargs["updated_at"] = datetime.now()
        updated_rows = instance.update(kwargs)

        if updated_rows <= 0:
            raise ("No matches found")

        self.session.commit()
        return instance.one()

    def delete(self, id):
        if id is None:
            raise ("id is none")

        if id < 0:
            raise ("id can not be null")

        instance = self.read_one(id)
        self.session.delete(instance)
        self.session.commit()
        return instance

    def get_item(self, instance):
        """
            Returns a public view of the resource
        """
        public_item = {"id": instance.id}

        return public_item

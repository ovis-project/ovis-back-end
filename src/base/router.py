"""
    Base.Router has a common inmplementation for routers
"""

from flask_restful import Resource
from flask_restful import reqparse
import traceback
from http.client import HTTPException


class Router(Resource):
    def __init__(self, storage, Controller):
        self.storage = storage
        self.base = storage.get_base()
        self.session = storage.get_session()
        self.connection = storage.get_connection()
        self.controller = Controller(self.base, self.session, self.connection)

        # TODO: check if the controller argument is `isinstance` of Controller

    def get(self, **kwargs):
        """
            Returns the a serialized version of an instance of the resource or
            a list of instances
        """
        parser = reqparse.RequestParser()
        parser.add_argument("per_page", type=int)
        parser.add_argument("page", type=int)
        args = parser.parse_args()

        if "resource_id" in kwargs:
            try:
                instance = self.controller.read_one(kwargs["resource_id"])
                content = self.controller.get_item(instance)
                response = {"data": content}
                return response
            except Exception as ex:
                traceback.print_exception(type(ex), ex, ex.__traceback__)

        else:
            try:
                list_of_instances = self.controller.read_all(**args)
                count_all = self.controller.count_all()
                content = list(map(self.controller.get_item, list_of_instances))
                response = {"data": content, "total": count_all}
                return response
            except Exception as ex:
                traceback.print_exception(type(ex), ex, ex.__traceback__)
                raise ex

    def post(self, **kwargs):
        """
            Creates a new instance of the resource
        """

        try:
            instance = self.controller.create(**kwargs)
            content = self.controller.get_item(instance)
            response = {"data": content}
            return response
        except Exception as ex:
            traceback.print_exception(type(ex), ex, ex.__traceback__)
            raise ex

    def put(self, resource_id, args):
        """
            Updates an instance of the resource given an id
        """
        try:
            instance = self.controller.update(resource_id, **args)
            content = self.controller.get_item(instance)
            response = {"data": content}
            return response
        except Exception as ex:
            traceback.print_exception(type(ex), ex, ex.__traceback__)
            raise HTTPException(ex)

    def delete(self, **kwargs):
        """
            Deletes an instance  of the resource given an id
        """
        if kwargs["resource_id"] is None:
            raise Exception("No id is given - 404")

        try:
            deleted = self.controller.delete(kwargs["resource_id"])
            content = self.controller.get_item(deleted)
            response = {"data": content}
            return response
        except Exception as ex:
            traceback.print_exception(type(ex), ex, ex.__traceback__)
            raise ex

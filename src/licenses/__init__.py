"""
Licences module

Contains the licence's types and licences management

"""

from people import partner, partnerController

from .licensetypes.controller import LicenseTypeController
from .licensetypes.routes import get_resource as get_license_type_resource
from .licensetypes.licensetype import LicenseType

from .licensedocument.controller import LicenseDocumentController
from .licensedocument.routes import get_resource as get_license_document_resource
from .licensedocument.licensedocument import LicenseDocument

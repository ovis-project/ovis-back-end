from .licensedocument import LicenseDocument
from ..licensetypes.controller import LicenseTypeController
from base import Controller
from datetime import datetime


class LicenseDocumentController(Controller):
    def __init__(self, base, session, conn):
        super().__init__(LicenseDocument, base, session, conn)
        self.license_type_controller = LicenseTypeController(base, session, conn)

    def get_item(self, license_document):
        try:
            parsed_date = license_document.expiration_date.isoformat()
        except:
            parsed_date = None

        public_item = {
            "id": license_document.id,
            "license_type_id": license_document.license_type,
            "truckowner_licensee": license_document.truckowner_licensee,
            "truck_licensee": license_document.truck_licensee,
            "driver_licensee": license_document.driver_licensee,
            "expiration_date": parsed_date,
        }

        try:
            license_type = self.license_type_controller.read_one(
                license_document.license_type
            )
            public_item["license_type"] = self.license_type_controller.get_item(
                license_type
            )
        except Exception as e:
            license_type = None

        try:
            public_item["created_at"] = license_document.created_at.isoformat()
            public_item["updated_at"] = license_document.updated_at.isoformat()
        except:
            public_item["created_at"] = None
            public_item["updated_at"] = None

        return public_item

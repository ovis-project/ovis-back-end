"""
  A license is a single license document.
  It has a type and an expiration date.

  It will be related to an owner, driver or truck
"""

from sqlalchemy import Column, Integer, String, ForeignKey, Date, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from storage import SQLiteStrategy

# from .. import LicenseType

Base = SQLiteStrategy.get_instance().get_base()


class LicenseDocument(Base):
    __tablename__ = "license_documents"

    id = Column(Integer, primary_key=True)
    expiration_date = Column(Date, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    # declare relationships
    license_type = Column(Integer, ForeignKey("license_types.id", ondelete="cascade"))

    # TODO: the licenses relationships need a better structure
    truck_licensee = Column(Integer, ForeignKey("trucks.id", ondelete="cascade"))
    driver_licensee = Column(Integer, ForeignKey("drivers.id", ondelete="cascade"))
    truckowner_licensee = Column(
        Integer, ForeignKey("truckowners.id", ondelete="cascade")
    )

    def __init__(self, **kwargs):
        self.expiration_date = kwargs["expiration_date"]

        if "license_type" in kwargs:
            self.license_type = kwargs["license_type"]

        if "truck_licensee" in kwargs:
            self.truck_licensee = kwargs["truck_licensee"]

        if "driver_licensee" in kwargs:
            self.driver_licensee = kwargs["driver_licensee"]

        if "truckowner_licensee" in kwargs:
            self.truckowner_licensee = kwargs["truckowner_licensee"]

        if "updated_at" in kwargs:
            self.updated_at = kwargs["updated_at"]

        if "created_at" in kwargs:
            self.created_at = kwargs["created_at"]

    def __repr__(self):
        return """
            <Licence(id={}, exp. date)>
        """.format(
            self.id, self.expiration_date
        )

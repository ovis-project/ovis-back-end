from .controller import LicenseDocumentController
from flask_restful import reqparse
from base import Router
from sqlalchemy import MetaData
from dateutil.parser import parse as dateutilparser


def get_resource(storage):

    engine = storage.get_connection()
    existing_tables = MetaData(bind=engine, reflect=True).tables

    if not "license_documents" in existing_tables:
        MetaData().create_all(engine)

    class LicenseDocumentResource(Router):
        def __init__(self):
            super().__init__(storage, LicenseDocumentController)

        """
        @api /api/licenses test
        """

        def post(self):
            parser = reqparse.RequestParser()
            parser.add_argument(
                "license_type", type=int, required=True, help="license_type is required"
            )
            parser.add_argument(
                "expiration_date",
                type=str,
                required=True,
                help="expiration_date is required",
            )
            parser.add_argument("truck_licensee", type=int)
            parser.add_argument("truckowner_licensee", type=int)
            parser.add_argument("driver_licensee", type=int)
            args = parser.parse_args()

            parsed_expiration_date = dateutilparser(args["expiration_date"])
            args["expiration_date"] = parsed_expiration_date
            return super().post(**args)

        def put(self, **kwargs):
            parser = reqparse.RequestParser()
            parser.add_argument("license_type", type=int)
            parser.add_argument("expiration_date", type=str)
            parser.add_argument("truckowner_licensee", type=int)
            parser.add_argument("truck_licensee", type=int)
            parser.add_argument("driver_licensee", type=int)
            args = parser.parse_args()

            parsed_expiration_date = dateutilparser(args["expiration_date"])
            args["expiration_date"] = parsed_expiration_date
            return super().put(kwargs["resource_id"], args)

    return LicenseDocumentResource

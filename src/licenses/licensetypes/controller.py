from base import Controller
from .licensetype import LicenseType


class LicenseTypeController(Controller):
    def __init__(self, base, session, conn):
        super().__init__(LicenseType, base, session, conn)

    def get_item(self, licensetype):
        public_item = {
            "id": licensetype.id,
            "title": licensetype.title,
            "description": licensetype.description,
            "is_required": licensetype.is_required,
            "related_resource": licensetype.related_resource,
            "created_at": licensetype.created_at.isoformat(),
            "updated_at": licensetype.updated_at.isoformat(),
        }

        return public_item

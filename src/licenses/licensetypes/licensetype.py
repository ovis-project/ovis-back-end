"""
  A license type is a type of a license document.

  Licenses can be grouped by their type in order to figure out whether there is
  a least on active document of a certain type.
"""

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Text
from sqlalchemy.orm import relationship
from storage import SQLiteStrategy

Base = SQLiteStrategy.get_instance().get_base()


class LicenseType(Base):
    __tablename__ = "license_types"

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    description = Column(Text)
    related_resource = Column(String, nullable=False)
    is_required = Column(Boolean, nullable=False, default=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    licenses = relationship("LicenseDocument")

    def __init__(self, **kwargs):

        if kwargs["title"] is not None:
            self.title = kwargs["title"]

        if kwargs["related_resource"] is not None:
            self.related_resource = kwargs["related_resource"]

        if kwargs["description"] is not None:
            self.description = kwargs["description"]

        if kwargs["is_required"] is not None:
            self.is_required = kwargs["is_required"]

        if kwargs["created_at"] is not None:
            self.created_at = kwargs["created_at"]

        if kwargs["updated_at"] is not None:
            self.updated_at = kwargs["updated_at"]

    def __repr__(self):
        return """
        <LicenseType(id={}, title={}, )>
      """.format(
            self.id, self.title
        )

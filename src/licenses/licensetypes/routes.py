from flask_restful import reqparse
from base import Router
from sqlalchemy import MetaData
from .controller import LicenseTypeController


def get_resource(storage):

    engine = storage.get_connection()
    existing_tables = MetaData(bind=engine, reflect=True).tables

    if not "licencetypes" in existing_tables:
        print("licencetypes not found")
        MetaData().create_all(engine)

    class LicenseTypeResource(Router):
        def __init__(self):
            super().__init__(storage, LicenseTypeController)

        def post(self):
            parser = reqparse.RequestParser()
            parser.add_argument(
                "title", type=str, required=True, help="title is required"
            )
            parser.add_argument("description", type=str)
            parser.add_argument("is_required", type=str)
            parser.add_argument("related_resource", type=str)
            args = parser.parse_args()

            args.is_required = args.is_required == "True"
            return super().post(**args)

        def put(self, **kwargs):
            parser = reqparse.RequestParser()
            parser.add_argument("title", type=str)
            parser.add_argument("description", type=str)
            parser.add_argument("is_required", type=str)
            parser.add_argument("related_resource", type=str)
            args = parser.parse_args()

            args.is_required = args.is_required == "True"
            return super().put(kwargs["resource_id"], args)

    return LicenseTypeResource

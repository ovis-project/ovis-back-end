from .partners.partner import partner
from .partners.routes import get_resource as get_partner_resource
from .partners.routes import partnerController

from .drivers.driver import Driver
from .drivers.controller import DriverController
from .drivers.routes import get_resource as get_driver_resource

from .truckowners.truckowner import TruckOwner
from .truckowners.controller import TruckOwnerController
from .truckowners.routes import get_resource as get_truck_owner_resource

from base import Controller
from .driver import Driver
from licenses import LicenseDocumentController
from people import partnerController


class DriverController(Controller):
    def __init__(self, base, session, conn):
        super().__init__(Driver, base, session, conn)
        self.licenseDocumentController = LicenseDocumentController(base, session, conn)
        self.partnerController = partnerController(base, session, conn)

    def create(self, **kwargs):
        if "partner_id" in kwargs:
            partner = self.partnerController.read_one(kwargs["partner_id"])
            kwargs["partner"] = partner
            return super().create(**kwargs)

    def assign_license(self, **kwargs):
        driver = self.read_one(kwargs["resource_id"])
        license_document = self.licenseDocumentController.create(**kwargs)
        driver.licenses.append(license_document)
        self.session.commit()
        print(
            "license_document: ",
            self.licenseDocumentController.get_item(license_document),
        )
        print("driver: ", self.get_item(driver))
        return self.get_item(driver)

    def get_item(self, driver):
        """
            Returns a public view of the resource
        """

        if driver is None:
            raise (Exception("driver parameter is none"))

        public_item = {
            "id": driver.id,
            "partner_id": driver.partner_id,
            "isActive": driver.isActive,
        }

        try:
            public_item["created_at"] = driver.created_at.isoformat()
            public_item["updated_at"] = driver.updated_at.isoformat()
        except:
            public_item["created_at"] = None
            public_item["updated_at"] = None

        try:
            public_item["person"] = driver.person
        except:
            public_item["person"] = None

        public_item["partner"] = self.partnerController.get_item(driver.partner)
        public_item["licenses"] = list(
            map(self.licenseDocumentController.get_item, driver.licenses)
        )

        return public_item

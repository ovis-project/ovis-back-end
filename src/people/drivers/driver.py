"""
  A partner is person who is working with the user and who's licenses should
  be managed by the system
"""

from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from storage import SQLiteStrategy

Base = SQLiteStrategy.get_instance().get_base()


class Driver(Base):
    __tablename__ = "drivers"

    id = Column(Integer, primary_key=True)
    isActive = Column(Boolean, default=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    # declare partner relationship
    partner_id = Column(Integer, ForeignKey("partners.id", ondelete="cascade"))
    partner = relationship("partner")

    # declare licenses relationships
    licenses = relationship("LicenseDocument")

    def __init__(self, **kwargs):
        self.partner_id = kwargs["partner_id"]

        if "isActive" in kwargs:
            self.isActive = kwargs["isActive"]

        if "partner" in kwargs:
            self.owner = kwargs["partner"]

        if "created_at" in kwargs:
            self.created_at = kwargs["created_at"]

        if "updated_at" in kwargs:
            self.updated_at = kwargs["updated_at"]

    def __repr__(self):
        return """ <Driver (id={})> """.format(self.id)

from base import Controller
from sqlalchemy import text
from .partner import partner


class partnerController(Controller):
    def __init__(self, base, session, conn):
        super().__init__(partner, base, session, conn)

    def get_not_drivers(self):
        cursor = self.db_connection.execute(
            text(
                "select d.partner_id from drivers d  join partners p on d.partner_id = p.id"
            )
        )
        partners_who_are_drivers = [row[0] for row in cursor]
        return (
            self.session.query(self.resource)
            .filter(self.resource.id.notin_(set(partners_who_are_drivers)))
            .all()
        )

    def get_not_truck_owners(self):
        cursor = self.db_connection.execute(
            text(
                "select t.partner_id from truckowners t join partners p on t.partner_id = p.id"
            )
        )
        partners_who_are_truck_owners = [row[0] for row in cursor]
        return (
            self.session.query(self.resource)
            .filter(self.resource.id.notin_(set(partners_who_are_truck_owners)))
            .all()
        )

    def get_item(self, partner):
        """
            Returns a public view of the resource
        """
        public_item = {
            "id": partner.id,
            "first_name": partner.first_name,
            "last_name": partner.last_name,
            "nick_name": partner.nick_name,
            "phone": partner.phone,
            "email": partner.email,
            "address": partner.address,
            "created_at": partner.created_at.isoformat(),
            "updated_at": partner.updated_at.isoformat(),
        }

        return public_item

"""
  A partner is person who is working with the user and who's licenses should
  be managed by the system
"""

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from storage import SQLiteStrategy


Base = SQLiteStrategy.get_instance().get_base()


class partner(Base):
    __tablename__ = "partners"

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    nick_name = Column(String)
    phone = Column(String)
    email = Column(String)
    address = Column(String)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def __init__(self, **kwargs):
        self.first_name = kwargs["first_name"]
        self.last_name = kwargs["last_name"]

        if kwargs["nick_name"] is not None:
            self.nick_name = kwargs["nick_name"]

        if kwargs["phone"] is not None:
            self.phone = kwargs["phone"]

        if kwargs["email"] is not None:
            self.email = kwargs["email"]

        if kwargs["address"] is not None:
            self.address = kwargs["address"]

        if kwargs["created_at"] is not None:
            self.created_at = kwargs["created_at"]

        if kwargs["updated_at"] is not None:
            self.updated_at = kwargs["updated_at"]

    def __repr__(self):
        return """
        <partner(id={}, first_name={}, last_name={}, nick_name={}, phone={}, email={}, address={})>
      """.format(
            self.id,
            self.first_name,
            self.last_name,
            self.nick_name,
            self.phone,
            self.email,
            self.address,
        )

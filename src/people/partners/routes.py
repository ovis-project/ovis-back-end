from flask_restful import reqparse
from base import Router
from sqlalchemy import MetaData
from .controller import partnerController


def get_resource(storage):

    engine = storage.get_connection()
    existing_tables = MetaData(bind=engine, reflect=True).tables

    if not "partners" in existing_tables:
        print("partners not found")
        MetaData().create_all(engine)

    class partnerResource(Router):
        def __init__(self):
            super().__init__(storage, partnerController)

        def get(self, **kwargs):
            parser = reqparse.RequestParser()
            parser.add_argument("is_not_driver", type=bool)
            parser.add_argument("is_not_truckowner", type=bool)
            parser.add_argument("resource_id", type=int)

            args = parser.parse_args()

            if args["is_not_driver"]:
                list_of_instances = self.controller.get_not_drivers()
            elif args["is_not_truckowner"]:
                list_of_instances = self.controller.get_not_truck_owners()
            else:
                # return all
                return super().get(**kwargs)

            content = list(map(self.controller.get_item, list_of_instances))
            response = {"data": content, "total": len(content)}

            return response

        def post(self):
            parser = reqparse.RequestParser()
            parser.add_argument(
                "first_name", type=str, required=True, help="first_name is required"
            )
            parser.add_argument(
                "last_name", type=str, required=True, help="last_name is required"
            )
            parser.add_argument("nick_name", type=str)
            parser.add_argument("phone", type=str)
            parser.add_argument("email", type=str)
            parser.add_argument("address", type=str)
            parser.add_argument("nick_name", type=str)
            args = parser.parse_args()

            return super().post(**args)

        def put(self, **kwargs):
            parser = reqparse.RequestParser()
            parser.add_argument("first_name", type=str, required=True)
            parser.add_argument("last_name", type=str)
            parser.add_argument("nick_name", type=str)
            parser.add_argument("phone", type=str)
            parser.add_argument("email", type=str)
            parser.add_argument("address", type=str)
            parser.add_argument("nick_name", type=str)
            args = parser.parse_args()

            return super().put(kwargs["resource_id"], args)

    return partnerResource

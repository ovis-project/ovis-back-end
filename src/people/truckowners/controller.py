from base import Controller
from .truckowner import TruckOwner
from licenses import LicenseDocumentController
from people import partnerController


class TruckOwnerController(Controller):
    def __init__(self, base, session, conn):
        super().__init__(TruckOwner, base, session, conn)
        self.licenseDocumentController = LicenseDocumentController(base, session, conn)
        self.partnerController = partnerController(base, session, conn)

    def create(self, **kwargs):
        if "partner_id" in kwargs:
            partner = self.partnerController.read_one(kwargs["partner_id"])
            kwargs["partner"] = partner
            return super().create(**kwargs)

    def assign_license(self, **kwargs):
        truckowner = self.read_one(kwargs["resource_id"])
        license_document = self.licenseDocumentController.create(**kwargs)
        truckowner.licenses.append(license_document)
        self.session.commit()
        return self.get_item(truckowner)

    def get_item(self, truck_owner):
        """
            Returns a public view of the resource
        """

        if truck_owner is None:
            raise (Exception("truck_owner parameter is none"))

        public_item = {
            "id": truck_owner.id,
            "partner_id": truck_owner.partner_id,
            "isActive": truck_owner.isActive,
        }

        try:
            public_item["created_at"] = driver.created_at.isoformat()
            public_item["updated_at"] = driver.updated_at.isoformat()
        except:
            public_item["created_at"] = None
            public_item["updated_at"] = None

        public_item["partner"] = self.partnerController.get_item(truck_owner.partner)
        public_item["licenses"] = list(
            map(self.licenseDocumentController.get_item, truck_owner.licenses)
        )

        return public_item

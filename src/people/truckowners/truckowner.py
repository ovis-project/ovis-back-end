"""
  A Truck Owner is person who has a Truck as their ownership
"""

from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from storage import SQLiteStrategy

Base = SQLiteStrategy.get_instance().get_base()


class TruckOwner(Base):
    __tablename__ = "truckowners"

    id = Column(Integer, primary_key=True)
    isActive = Column(Boolean, default=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    # declare partner relationship
    partner_id = Column(Integer, ForeignKey("partners.id", ondelete="cascade"))
    partner = relationship("partner")

    # declare licenses relationship
    licenses = relationship("LicenseDocument")

    def __init__(self, **kwargs):
        self.partner_id = kwargs["partner_id"]

        if "isActive" in kwargs:
            self.isActive = kwargs["isActive"]

        if "partner_id" in kwargs:
            self.partner_id = kwargs["partner_id"]

        if "partner" in kwargs:
            self.partner = kwargs["partner"]

        if "created_at" in kwargs:
            self.created_at = kwargs["created_at"]

        if "updated_at" in kwargs:
            self.updated_at = kwargs["updated_at"]

    def __repr__(self):
        return """
        <TruckOwner (id={})>
      """.format(
            self.id
        )

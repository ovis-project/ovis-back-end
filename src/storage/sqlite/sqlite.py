from .. import StorageStrategy


class SQLiteStrategy(StorageStrategy):

    __instance = None

    @staticmethod
    def get_instance():
        if SQLiteStrategy.__instance == None:
            print("instance is none")
            SQLiteStrategy()

        return SQLiteStrategy.__instance

    @staticmethod
    def initialize(url):
        SQLiteStrategy.__instance.url = url

    def __init__(self):
        if SQLiteStrategy.__instance != None:
            print("will throw ")
            raise Exception("This class is a singleton!")
        else:
            print("initializing instance")
            super().__init__()
            SQLiteStrategy.__instance = self
            SQLiteStrategy.__instance.protocol = "sqlite"

    def get_connection(self):
        return super().get_connection()

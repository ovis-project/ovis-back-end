from sqlalchemy import create_engine
from abc import ABC, abstractmethod
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


class StorageStrategy(ABC):
    def __init__(self):
        self.engine = None
        self.base = None
        self.url = None
        self.protocol = None

    def get_connection(self):

        if self.url is None:
            raise ("The url is not specified")

        if self.engine is None:
            url = "{}:///{}".format(self.protocol, self.url)
            self.engine = create_engine(url)
            print("DB initialized following the {} strategy".format(self.protocol))

        return self.engine

    def get_session(self):
        if self.engine is None:
            raise "Engine is not set up"

        DBSession = sessionmaker(bind=self.engine)
        return DBSession()

    def get_base(self):

        if self.base is None:
            self.base = declarative_base()

        return self.base

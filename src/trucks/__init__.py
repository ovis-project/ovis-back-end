from .trucks.routes import get_resource as get_trucks_resource
from .trucks.controller import TruckController
from .trucks.truck import Truck

from licenses import LicenseDocumentController

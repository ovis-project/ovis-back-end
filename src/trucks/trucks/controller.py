from base import Controller
from .truck import Truck
from people import Driver, DriverController, TruckOwner, TruckOwnerController
from licenses import LicenseDocumentController


class TruckController(Controller):
    def __init__(self, base, session, conn):
        super().__init__(Truck, base, session, conn)
        self.driverController = DriverController(base, session, conn)
        self.truckOwnerController = TruckOwnerController(base, session, conn)
        self.licenseDocumentController = LicenseDocumentController(base, session, conn)

    def create(self, **kwargs):
        if "driver_id" in kwargs:
            driver = self.driverController.read_one(kwargs["driver_id"])
            kwargs["driver"] = driver

        if "owner_id" in kwargs:
            owner = self.truckOwnerController.read_one(kwargs["owner_id"])
            kwargs["owner"] = owner

        return super().create(**kwargs)

    def assign_license(self, **kwargs):
        print("id: ", kwargs["resource_id"])
        truck = self.read_one(kwargs["resource_id"])
        license_document = self.licenseDocumentController.create(**kwargs)
        truck.licenses.append(license_document)
        self.session.commit()
        return self.get_item(truck)

    def get_item(self, truck):
        """
            Returns a public view of the resource
        """
        public_item = {
            "id": truck.id,
            "license_number": truck.license_number,
            "alias": truck.alias,
            "driver_id": truck.driver_id,
            "owner_id": truck.owner_id,
            "created_at": truck.created_at.isoformat(),
            "updated_at": truck.updated_at.isoformat(),
        }

        try:
            driver = self.driverController.get_item(truck.driver)
            public_item["driver"] = driver
        except:
            public_item["driver"] = None

        try:
            owner = self.truckOwnerController.get_item(truck.owner)
            public_item["owner"] = owner
        except:
            public_item["owner"] = None

        try:
            public_item["licenses"] = list(
                map(self.licenseDocumentController.get_item, truck.licenses)
            )
        except:
            public_item["licenses"] = []

        return public_item

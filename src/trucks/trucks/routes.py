from flask_restful import reqparse
from base import Router
from sqlalchemy import MetaData
from .controller import TruckController
from dateutil.parser import parse as dateutilparser


def get_resource(storage):

    engine = storage.get_connection()
    existing_tables = MetaData(bind=engine, reflect=True).tables

    if not "trucks" in existing_tables:
        print("trucks not found")
        MetaData().create_all(engine)

    class TruckResource(Router):
        def __init__(self):
            super().__init__(storage, TruckController)

        def post(self, **kwargs):
            if "resource_id" in kwargs:
                parser = reqparse.RequestParser()
                parser.add_argument("expiration_date", type=str)
                parser.add_argument("license_type", type=int)
                parser.add_argument("title", type=str)
                args = parser.parse_args()

                args["resource_id"] = kwargs["resource_id"]
                parsed_expiration_date = dateutilparser(args["expiration_date"])
                args["expiration_date"] = parsed_expiration_date

                return self.controller.assign_license(**args)
            else:
                parser = reqparse.RequestParser()
                parser.add_argument(
                    "license_number",
                    type=str,
                    required=True,
                    help="license_number is required",
                )
                parser.add_argument("driver_id", type=int)
                parser.add_argument("owner_id", type=int)
                parser.add_argument("alias", type=str)
                args = parser.parse_args()

                return super().post(**args)

        def put(self, **kwargs):
            parser = reqparse.RequestParser()
            parser.add_argument("license_number", type=str)
            parser.add_argument("driver_id", type=int)
            parser.add_argument("owner_id", type=int)
            parser.add_argument("alias", type=str)
            args = parser.parse_args()

            return super().put(kwargs["resource_id"], args)

    return TruckResource

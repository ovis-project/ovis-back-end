"""
  A partner is person who is working with the user and who's licenses should
  be managed by the system
"""

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from storage import SQLiteStrategy


Base = SQLiteStrategy.get_instance().get_base()


class Truck(Base):
    __tablename__ = "trucks"

    id = Column(Integer, primary_key=True)
    license_number = Column(String, nullable=False)
    alias = Column(String)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    # declare driver relationship
    driver_id = Column(Integer, ForeignKey("drivers.id", ondelete="set null"))
    driver = relationship("Driver")

    # declare owner relationship
    owner_id = Column(Integer, ForeignKey("truckowners.id", ondelete="set null"))
    owner = relationship("TruckOwner")

    # declare licenses relationships
    licenses = relationship("LicenseDocument")

    def __init__(self, **kwargs):
        self.license_number = kwargs["license_number"]

        if "alias" in kwargs:
            self.alias = kwargs["alias"]

        if "driver_id" in kwargs:
            self.driver_id = kwargs["driver_id"]

        if "driver" in kwargs:
            self.driver = kwargs["driver"]

        if "owner_id" in kwargs:
            self.owner_id = kwargs["owner_id"]

        if "owner" in kwargs:
            self.owner = kwargs["owner"]

        if "created_at" in kwargs:
            self.created_at = kwargs["created_at"]

        if "updated_at" in kwargs:
            self.updated_at = kwargs["updated_at"]

    def __repr__(self):
        return """
        <Truck(id={}, license_number={}, alias={})>
      """.format(
            self.id, self.license_number, self.alias
        )

"""
Users module

Contains the user management, authentication and authorization modules

"""

from .user.controller import UserController
from .user.routes import get_resource

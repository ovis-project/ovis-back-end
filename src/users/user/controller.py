from base import Controller
from sqlalchemy import MetaData
from datetime import datetime
from .user import User


class UserController(Controller):
    def __init__(self, base, session, conn):
        super().__init__(User, base, session, conn)

    def create(self, **kwargs):
        if kwargs["password"] != kwargs["password_repeat"]:
            raise (Exception("Passwords does not match"))

        # password_repeat is not used in the model
        del kwargs["password_repeat"]
        return super().create(**kwargs)

    def get_item(self, user):
        public_item = {
            "id": user.id,
            "username": user.username,
            "email": user.email,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "created_at": user.created_at.isoformat(),
            "updated_at": user.updated_at.isoformat(),
        }

        return public_item

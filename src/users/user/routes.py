from base import Router
from flask import Flask
from flask_restful import Resource, reqparse
from .controller import UserController
import json


def get_resource(storage):
    class UserResource(Router):
        def __init__(self):
            self.storage = storage
            self.base = storage.get_base()
            self.session = storage.get_session()
            self.connection = storage.get_connection()
            self.controller = UserController(self.base, self.session, self.connection)

        def post(self):
            parser = reqparse.RequestParser()
            parser.add_argument(
                "username", type=str, required=True, help="username is required"
            )
            parser.add_argument(
                "email", type=str, required=True, help="email is required"
            )
            parser.add_argument(
                "password", type=str, required=True, help="password is required"
            )
            parser.add_argument(
                "password_repeat",
                type=str,
                required=True,
                help="password_repeat is required",
            )
            parser.add_argument("first_name", type=str)
            parser.add_argument("last_name", type=str)
            args = parser.parse_args()

            return super().post(**args)

        def put(self, **kwargs):
            parser = reqparse.RequestParser()
            parser.add_argument("email", type=str)
            parser.add_argument("first_name", type=str)
            parser.add_argument("last_name", type=str)
            args = parser.parse_args()

            return super().put(kwargs["resource_id"], args)

    return UserResource

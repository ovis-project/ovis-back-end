from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from storage import SQLiteStrategy

Base = SQLiteStrategy.get_instance().get_base()


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    username = Column(String, nullable=False)
    email = Column(String, nullable=False)
    password = Column(String, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def __init__(
        self,
        username,
        email,
        password="",
        first_name="",
        last_name="",
        created_at=None,
        updated_at=None,
    ):
        self.username = username
        self.email = email
        self.password = password
        self.first_name = first_name
        self.last_name = last_name

        if created_at is not None:
            self.created_at = created_at

        if updated_at is not None:
            self.updated_at = updated_at

    def __repr__(self):
        return """
        <User(first_name={}, last_name={}, username={}, email={})>
      """.format(
            self.first_name, self.last_name, self.username, self.email
        )
